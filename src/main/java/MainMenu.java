import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Formatter;


public class MainMenu {
	static Scanner sc = new Scanner(System.in);
	private static String DB_URL = SQLDatabaseConnection.getDB_URL();
	private static String USER_NAME = SQLDatabaseConnection.getUSER_NAME();
	private static String PASSWORD = SQLDatabaseConnection.getPASSWORD();
	
	public static void menu() {
		System.out.println("\n---------------------MENU----------------------\n"
				+ "1. All account information\n"
				+ "2. 1 account information\n" 
				+ "3. Search account \n"
						+ "4. Add new account \n"
						+ "5. Update account information \n"
						+ "6. Delete Account\n"
						+ "7. Import CSV file\n"
						+ "8. Export Account list to csv file");

	}
	
	
	public static int getInt(Scanner sc1) {
		int a;
		while (true) {
			try {
				sc1 = new Scanner(System.in);
				a = sc1.nextInt();
				if (a == 1 || a == 2 || a == 3 || a == 4 || a ==5 || a == 6 || a== 7 || a == 8) {
					break;
				} else {
					System.out.println("Invalid answer. Please enter your choice from 1 to 8: ");
				}
			} catch (Exception e) {
				System.out.println("Invalid data type. Please enter an integer");
			}
		}
		return a;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {

			Connection conn = SQLDatabaseConnection.getConnection(DB_URL, USER_NAME, PASSWORD);

			// create statement
			Statement stmt = conn.createStatement();
			Account acc = new Account();

			int options;
			ResultSet rs = null;

			while (true) {
				menu();
				options = getInt(sc);
				ArrayList<Account> accounts;
				
				switch(options){
					case 1:
						accounts = AccountService.getAccountList(rs, stmt);
						System.out.println("Accounts: ");
						System.out.println("---------------------------------------------------------------------------------------");
						System.out.println("| id |    account_id  |             full_name          |      address       |is_active|");
						System.out.println("---------------------------------------------------------------------------------------");

						for (Account a: accounts) {
							System.out.format("|%4d|%16d|%32s|%20s|%9d|", a.getId(), a.getAccountId(), a.getFullName(), a.getAddress(), a.getIsActive());
							System.out.println("\n---------------------------------------------------------------------------------------");
						}
						break;
						
					case 2:
						String accountId1;

						System.out.println("Enter account id = ");
						accountId1 = InputUtils.getString();
						System.out.println("Account #" + accountId1);
						Account account = AccountService.getSpecificAccount(rs, stmt, accountId1);

						if (account == null){
							System.out.println("");
						} else {
							System.out.println("\n---------------------------------------------------------------------------------------");
							System.out.println("| id |    account_id  |             full_name          |      address       |is_active|");
							System.out.println("\n---------------------------------------------------------------------------------------");
							System.out.format("|%4d|%16d|%32s|%20s|%9d|", account.getId(), account.getAccountId(), account.getFullName(), account.getAddress(), account.getIsActive());
							System.out.println("\n---------------------------------------------------------------------------------------");
						}

						break;
						
					case 3:
						System.out.println("---------------Search by full name------------------");
						System.out.println("Enter full name = ");
						String fullName = InputUtils.getString();
						accounts = AccountService.search(fullName, rs, stmt);

						for (Account a: accounts) {
							System.out.println(a);
							System.out.println("Result: ");
							System.out.println("---------------------------------------------------------------------------------------");
							System.out.println("| id |    account_id  |             full_name          |      address       |is_active|");
							System.out.println("---------------------------------------------------------------------------------------");
							System.out.format("|%4d|%16d|%32s|%20s|%9d|", a.getId(), a.getAccountId(), a.getFullName(), a.getAddress(), a.getIsActive());
							System.out.println("\n---------------------------------------------------------------------------------------");
						}
						break;
						
					case 4:
						if (AccountService.inputAccount(conn)) {
							System.out.println("Create account successful!");
						} else {
							System.out.println("Create account failed!");
						}
						break;
						
					case 5:
						if (AccountService.updateAccount(conn)) {
							System.out.println("Update account successful!");
						} else {
							System.out.println("Update account failed!");
						}
						break;
						
						
					case 6:
						if (AccountService.removeAccount(conn)) {
							System.out.println("Delete account successful!");
						} else {
							System.out.println("delete account failed!");
						}
						break;
						
					case 7:
						AccountService.importCSV("D:\\sql\\account.csv", conn);
						break;
						
					case 8:
						AccountService.exportDataToCsv("D:\\sql\\accountDbData.csv", rs, stmt);
						break;
						

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		sc.close();
		

	}


}
