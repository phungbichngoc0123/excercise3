import java.util.Scanner;

public class InputUtils {
	  private static Scanner sc = new Scanner(System.in);

	    public static String getString() {
	        return sc.nextLine();
	    }

	    public static int getInt() {
	    	int n = 0;
	        while(true) {
	            try {
	                n = Integer.valueOf(getString());// ep kieu nhap vao sang integer
	                break;
	            } catch (Exception e) {
	                System.out.println("Invalid, please try again");
	            }
	        }
	        return n;
	    }
}
