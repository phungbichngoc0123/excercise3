import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AccountService {

	public static ArrayList<Account> getAccountList(ResultSet rs, Statement stmt) {
		ArrayList<Account> accountList = new ArrayList<Account>();

		try {
			rs = stmt.executeQuery("select * from account order by id");
			
			while (rs.next()) {
				
				accountList.add(new Account(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4),
						rs.getInt(5)));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return accountList;
	}

	public static Account getSpecificAccount(ResultSet rs, Statement stmt, String accountId1) {
		Account account = null;

		try {
			rs = stmt.executeQuery("select * from account where account_id = '" + accountId1 + "'");
			// show data

			while (rs.next()) {
				account = new Account(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4),
						rs.getInt(5));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("There is no result");
		}

		return account;

	}

	public static ArrayList<Account> search(String fullName, ResultSet rs, Statement stmt) {

		ArrayList<Account> accountList = new ArrayList<Account>();


		try {
			rs = stmt.executeQuery("select * from account where full_name like '%" + fullName + "%'");
			// show data
			System.out.println("Result for \"" + fullName + "\"");
			while (rs.next()) {
				Account acc = new Account(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4),
						rs.getInt(5));
				accountList.add(acc);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return accountList;

	}



	public static boolean inputAccount(Connection conn) {

		System.out.println("Enter account id= ");
		int accountId = InputUtils.getInt();
		System.out.println("Enter fullname= ");
		String fullName = InputUtils.getString();
		System.out.println("Enter address= ");
		String address = InputUtils.getString();
		System.out.println("Enter isActiave= ");
		int isActive = InputUtils.getInt();

		int rowNum = 0;;

		String sql = "insert into account (account_id, full_name, address, is_active) values (?, ?, ?, ?)";
		//String sql= "insert into account (account_id, full_name, address, is_active) values (1, 'ngoc 1', 'hanoi1', 1),(2, 'ngoc 2', 'hanoi2', 1)";

		try {
			PreparedStatement ps = conn.prepareStatement(sql);

			//set param values
			ps.setInt(1, accountId);
			ps.setString(2, fullName);
			ps.setString(3, address);
			ps.setInt(4, isActive);

			rowNum = ps.executeUpdate();
			//execute query

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Unsuccessful! Invalid input info. Please try again");
		}

		if (rowNum >=1 ) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean updateAccount(Connection conn) {
		System.out.println("You want to update information for account id =  ");
		int accountId = InputUtils.getInt();

		System.out.println("Enter new fullname = ");
		String fullName = InputUtils.getString();

		System.out.println("Enter new address = ");
		String address = InputUtils.getString();

		int rowNum = 0;

		String sql = "update account set full_name = ?, address = ? where account_id = ?";

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, fullName);
			ps.setString(2, address);
			ps.setInt(3, accountId);

			rowNum = ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (rowNum >=1 ) {
			return true;
		} else {
			return false;
		}


	}

	public static boolean removeAccount(Connection conn) {
		System.out.println("Enter account id that you want to delete: ");
		int accountId = InputUtils.getInt();
		String sql = "update account set is_active = '2' where account_id = ?";

		int rowNum = 0;

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, accountId);
			rowNum = ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (rowNum >=1 ) {
			return true;
		} else {
			return false;
		}

	}
	
	public static void importCSV(String fileName, Connection conn) {
		
		//int batchSize = 20;
		try {
			conn.setAutoCommit(false);// allow to group muti subsequence statement under the same transaction
			
			String sql = "insert into account (account_id, full_name, address, is_active) values (?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			
			String lineText = null;
			int count = 0;
			
			br.readLine();// skip header line
			
			while((lineText = br.readLine()) != null){
				String[] data = lineText.split(",");
				String accountId = data[0];
				String fulname = data[1];
				String address = data[2];
				String isActive = data[3];
				
				try {
					int accountId1 = Integer.parseInt(accountId);
					ps.setInt(1, accountId1);
					
				} catch (Exception e) {
					//e.printStackTrace();
					System.out.println("Import data failed!");
				}
				ps.setString(2, fulname);
				ps.setString(3, address);
				int isActive1 = Integer.parseInt(isActive);
				ps.setInt(4, isActive1);
				
				ps.addBatch();
				ps.executeBatch();
				
			}
			br.close();
			conn.commit();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			System.out.println("successful import");
		}
		
	}
	
	public static void exportDataToCsv(String fileName, ResultSet rs, Statement stmt) {
		
		String sql = "select * from account";
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			//header
			bw.write("id,\taccount_id,\tfullName,\taddress,\tis_Active");
			
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				int id = rs.getInt("id");
				int accountId = rs.getInt("account_id");
				String fullName = rs.getString("full_name");
				String address = rs.getString("address");
				int isActive = rs.getInt("is_active");
				
				String line = String.format("%d, \t%d, \t%s, \t%s, \t%d", id, accountId, fullName, address, isActive);
				
				bw.newLine();
				bw.write(line);
				
			}
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println("Successul export!");
		}
		
		
	}

}
